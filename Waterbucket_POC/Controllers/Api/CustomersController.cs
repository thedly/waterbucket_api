﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using Waterbucket_POC.Models;

namespace Waterbucket_POC.Controllers.Api
{
    public class CustomersController : ApiController
    {
        private ApplicationDbContext _context;

        public CustomersController()
        {
            _context = new ApplicationDbContext();
        }

        // GET Orders
        public IEnumerable<Customer> GetCustomers()
        {
            return _context.Customers.ToList();
        }

        // GET Orders
        public Customer GetSellerById(int customersellerId)
        {
            var customer = _context.Customers.FirstOrDefault(sel => sel.Id == customersellerId);
            if (customer == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            return customer;
        }

        //POST api/sellers
        [HttpPost]
        public Customer CreateSeler(Customer _customer)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            _context.Customers.Add(_customer);
            _context.SaveChanges();
            return _customer;
        }

        [HttpPut]
        public void UpdateCustomer(int id, Customer _customer)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var customerInDb = _context.Customers.FirstOrDefault(cus => cus.Id == id);

            if (customerInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            customerInDb.Address1 = _customer.Address1;
            customerInDb.Address2 = _customer.Address2;
            customerInDb.City = _customer.City;
            customerInDb.Country = _customer.Country;
            customerInDb.Name = _customer.Name;
            customerInDb.Pincode = _customer.Pincode;
            customerInDb.State = _customer.State;

            _context.SaveChanges();
        }

        [HttpDelete]
        public void DeleteCustomer(int id)
        {
            var customerInDb = _context.Customers.FirstOrDefault(cus => cus.Id == id);
            if (customerInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            _context.Customers.Remove(customerInDb);
            _context.SaveChanges();
        }
    }
}