﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using Waterbucket_POC.Models;

namespace Waterbucket_POC.Controllers.Api
{
    public class SellersController: ApiController
    {
        private ApplicationDbContext _context;

        public SellersController()
        {
            _context = new ApplicationDbContext();
        }

        // GET Orders
        public IEnumerable<Seller> GetSellers()
        {
            return _context.Sellers.ToList();
        }

        // GET Orders
        public Seller GetSellerById(int sellerId)
        {
            var seller = _context.Sellers.FirstOrDefault(sel => sel.Id == sellerId);
            if (seller == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            return seller;
        }

        //POST api/sellers
        [HttpPost]
        public Seller CreateSeler(Seller _seller)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            _context.Sellers.Add(_seller);
            _context.SaveChanges();
            return _seller;
        }

        [HttpPut]
        public void UpdateOrder(int id, Seller _seller)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var sellerInDb = _context.Sellers.FirstOrDefault(sel => sel.Id == id);

            if (sellerInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            sellerInDb.Address1 = _seller.Address1;
            sellerInDb.Address2 = _seller.Address2;
            sellerInDb.City = _seller.City;
            sellerInDb.Country = _seller.Country;
            sellerInDb.Name = _seller.Name;
            sellerInDb.Pincode = _seller.Pincode;
            sellerInDb.State = _seller.State;

            _context.SaveChanges();
        }

        [HttpDelete]
        public void DeleteSeller(int id)
        {
            var sellerInDb = _context.Sellers.FirstOrDefault(sel => sel.Id == id);
            if (sellerInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            _context.Sellers.Remove(sellerInDb);
            _context.SaveChanges();
        }



    }
}
