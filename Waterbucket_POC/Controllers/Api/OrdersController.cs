﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Waterbucket_POC.Models;

namespace Waterbucket_POC.Controllers.Api
{
    public class OrdersController : ApiController
    {
        private ApplicationDbContext _context;

        public OrdersController()
        {
            _context = new ApplicationDbContext();
        }

        // GET Orders
        public IEnumerable<Order> GetOrders()
        {
            return _context.Orders.Include("Seller").Include("Customer").ToList();
        }

        // GET Orders
        public Order GetOrders(int orderId)
        {
            var order = _context.Orders.Include("Seller").Include("Customer").FirstOrDefault(ord => ord.Id == orderId);
            if (order == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            return order;
        }

        // GET Orders by customer id
        public IEnumerable<Order> GetOrderByCustomerId(int customerId)
        {
            return _context.Orders.Include("Seller").Include("Customer").Where(ord => ord.CustomerId == customerId).ToList();
        }

        // GET Orders by seller id
        public IEnumerable<Order> GetOrderBySellerId(int sellerId)
        {
            return _context.Orders.Include("Seller").Include("Customer").Where(ord => ord.SellerId == sellerId).ToList();
        }

        // GET Orders by seller id and customer id
        public IEnumerable<Order> GetOrderBySellerId(int sellerId, int customerId)
        {
            return _context.Orders.Include("Seller").Include("Customer").Where(ord => ord.SellerId == sellerId && ord.CustomerId == customerId).ToList();
        }

        //POST api/orders
        [HttpPost]
        public Order CreateOrder(Order _order)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            _context.Orders.Add(_order);
            _context.SaveChanges();
            return _order;
        }

        [HttpPut]
        public void UpdateOrder(int id, Order _order)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var orderInDb = _context.Orders.FirstOrDefault(ord => ord.Id == id);

            if (orderInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            orderInDb.CustomerId = _order.CustomerId;
            orderInDb.SellerId = _order.SellerId;
            orderInDb.TotalItems = _order.TotalItems;
            orderInDb.TotalPrice = _order.TotalPrice;
            orderInDb.TypeId = _order.TypeId;

            _context.SaveChanges();
        }

        public void DeleteCustomer(int id)
        {
            var orderInDb = _context.Orders.FirstOrDefault(ord => ord.Id == id);

            if (orderInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            _context.Orders.Remove(orderInDb);
            _context.SaveChanges();
        }



    }
}
