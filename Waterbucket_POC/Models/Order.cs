﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Waterbucket_POC.Models
{
    public class Order
    {
        public int Id { get; set; }
        public ContainerType Type { get; set; }
        public int TotalItems { get; set; }
        public Seller Seller { get; set; }
        public int SellerId { get; set; }
        public int TypeId { get; set; }
        public int TotalPrice { get; set; }
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }
    }
}