﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Waterbucket_POC.Models
{
    public class ContainerPrice
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}