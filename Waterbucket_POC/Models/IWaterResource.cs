﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Waterbucket_POC.Models
{
    public interface IWaterResource
    {
        ContainerType Type { get; set; }
        ContainerPrice Price { get; set; }
        ContainerQuantity Quantity { get; set; }
        Seller Seller { get; set; }
    }
}