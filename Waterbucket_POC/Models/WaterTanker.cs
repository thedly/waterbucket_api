﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Waterbucket_POC.Models
{
    public class WaterTanker : IWaterResource
    {
        public int Id { get; set; }
        public int TypeId { get; set; }
        public int PriceId { get; set; }
        public int QuantityId { get; set; }
        public int SellerId { get; set; }

        public ContainerType Type { get; set; }
        public ContainerPrice Price { get; set; }
        public ContainerQuantity Quantity { get; set; }
        public Seller Seller { get; set; }
    }
}