// <auto-generated />
namespace Waterbucket_POC.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class populateEntities : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(populateEntities));
        
        string IMigrationMetadata.Id
        {
            get { return "201606181629287_populateEntities"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
